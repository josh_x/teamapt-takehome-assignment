import { Box, Button } from '@mui/material';
import { styled } from '@mui/material/styles';
const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 2),
  ...theme.mixins.toolbar,
  justifyContent: 'space-between',
}));

const TaskWrapperStyle = styled(Button)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(3),
  justifyContent: 'space-between',
  backgroundColor: '#EDF1FB',
  width: '100%',
  borderRadius: '20px',
  cursor: 'pointer',
  textAlign: 'left',
}));

const TaskIconStyle = styled(Box)(() => ({
  borderRadius: '100%',
  width: '50px',
  height: '50px',
  backgroundColor: '#1b5afd',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: '#fff',
}));

export { DrawerHeader, TaskWrapperStyle, TaskIconStyle };
