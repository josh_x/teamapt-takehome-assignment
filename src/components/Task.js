import { useTheme } from '@mui/material/styles';
import plusOutline from '@iconify/icons-eva/plus-outline';
// iconify
import { Icon } from '@iconify/react';
import { Typography } from '@mui/material';
import PropTypes from 'prop-types';
import { TaskIconStyle, TaskWrapperStyle } from '../styles/sidebar.style';

NewTask.propTypes = {
  text: PropTypes.object,
};

export function NewTask({ text }) {
  const theme = useTheme();

  return (
    <TaskWrapperStyle>
      <Typography
        sx={{
          color: theme.palette.secondary.main,
        }}
      >
        {text}
      </Typography>
      <TaskIconStyle>
        <Icon icon={plusOutline} width={25} height={25} />
      </TaskIconStyle>
    </TaskWrapperStyle>
  );
}
