import { Link } from '@mui/material';
import PropTypes from 'prop-types';

TABreadcrumb.propTypes = {
  text: PropTypes.string.isRequired,
  sx: PropTypes.object,
};

export default function TABreadcrumb({ text, sx }) {
  return (
    <Link underline="none" color="inherit" sx={sx}>
      {text}
    </Link>
  );
}
