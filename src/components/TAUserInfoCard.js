import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { Avatar, Stack, Typography } from '@mui/material';
import PropTypes from 'prop-types';

TAUserInfoCard.propTypes = {
  src: PropTypes.string,
  username: PropTypes.string.isRequired,
  email: PropTypes.string,
};

export default function TAUserInfoCard({ src, username, email }) {
  return (
    <Stack
      direction="row"
      spacing={3}
      alignItems="center"
      sx={{ my: 'auto', px: 2, cursor: 'pointer' }}
    >
      <Avatar src={src}></Avatar>
      <Stack>
        <Typography>{username}</Typography>
        {email && (
          <Typography variant="caption" sx={{ color: 'text.disabled' }}>
            {email}
          </Typography>
        )}
      </Stack>
      <Typography sx={{ color: 'text.disabled' }}>
        <KeyboardArrowUpIcon />
      </Typography>
    </Stack>
  );
}
