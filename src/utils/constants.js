import DashboardIcon from '@mui/icons-material/Dashboard';
import AnalyticsIcon from '@mui/icons-material/Analytics';
import FolderIcon from '@mui/icons-material/Folder';
import GpsFixedIcon from '@mui/icons-material/GpsFixed';
import HistoryIcon from '@mui/icons-material/History';
import SettingsIcon from '@mui/icons-material/Settings';

export const DRAWER_WIDTH = 240;
export const COLLAPSE_WIDTH = 102;

export const APPBAR_MOBILE = 64;
export const APPBAR_DESKTOP = 92;

export const SIDEBAR_MENUS = [
  {
    label: 'Dashboard',
    icon: <DashboardIcon />,
  },
  {
    label: 'Analytics',
    icon: <AnalyticsIcon />,
  },
  {
    label: 'Projects',
    icon: <FolderIcon />,
  },
  {
    label: 'Tracking',
    icon: <GpsFixedIcon />,
  },
  {
    label: 'History',
    icon: <HistoryIcon />,
  },
  {
    label: 'Settings',
    icon: <SettingsIcon />,
  },
];
