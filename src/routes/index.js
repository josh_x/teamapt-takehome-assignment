import { useRoutes } from 'react-router-dom';
import DashboardLayout from '../layouts';
import Landing from '../pages/Landing';

export default function MetacareRouter() {
  return useRoutes([
    {
      path: '/',
      element: <DashboardLayout />,
      children: [{ path: '', element: <Landing /> }],
    },
  ]);
}
