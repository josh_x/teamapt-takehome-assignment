import { useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import PerpetualIbe from '../static/mock/images/avatar/avatar_2.jpg';
import Oluwasanmi from '../static/mock/images/avatar/avatar_3.jpg';
import Rhoda from '../static/mock/images/avatar/avatar_4.jpg';
import Ndebe from '../static/mock/images/avatar/avatar_5.jpg';
import plusOutline from '@iconify/icons-eva/plus-outline';
import { Icon } from '@iconify/react';
import {
  Avatar,
  AvatarGroup,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Typography,
} from '@mui/material';
import searchOutline from '@iconify/icons-eva/search-outline';
import { useState } from 'react';

export default function Landing() {
  const theme = useTheme();
  const [month, setMonth] = useState('jan');

  const handleChange = (event) => {
    setMonth(event.target.value);
  };

  return (
    <Stack direction="column" spacing={3}>
      <Stack direction="row" justifyContent="space-between">
        <Stack>
          <Typography variant="h5">GSE Banking App</Typography>
          <Typography variant="caption">56</Typography>
        </Stack>
        <Stack direction="row" spacing={3} alignItems="flex-start">
          <Button
            sx={{
              px: 2,
              border: '1px dashed grey',
              borderRadius: 3,
              color: theme.palette.secondary.main,
            }}
          >
            <Icon icon={plusOutline} width={25} height={25} />
            Invite
          </Button>
          <AvatarGroup max={4} total={18}>
            <Avatar alt="Perpetual Ibe" src={PerpetualIbe} />
            <Avatar alt="Oluwasanmi Akinrele" src={Oluwasanmi} />
            <Avatar alt="Rhoda Adeleke" src={Rhoda} />
            <Avatar alt=" Ndebe Wisdom" src={Ndebe} />
          </AvatarGroup>
        </Stack>
      </Stack>
      <Stack direction="row" justifyContent="space-between">
        <Stack spacing={2} direction="row" alignItems="center">
          <Typography>February 2021</Typography>
          <IconButton>
            <ChevronLeftIcon />
          </IconButton>
          <IconButton>
            <ChevronRightIcon />
          </IconButton>
        </Stack>
        <Stack spacing={2} direction="row">
          <IconButton>
            <Icon icon={searchOutline} width={25} height={25} />
          </IconButton>
          <FormControl>
            <InputLabel id="month-select-label">Month</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={month}
              label="Month"
              onChange={handleChange}
              sx={{ borderRadius: 10 }}
            >
              <MenuItem value="jan">January</MenuItem>
            </Select>
          </FormControl>
        </Stack>
      </Stack>
    </Stack>
  );
}
