import {
  createTheme,
  CssBaseline,
  StyledEngineProvider,
  ThemeProvider,
} from '@mui/material';

import PropTypes from 'prop-types';
import typography from './typography';

ThemeConfig.propTypes = {
  children: PropTypes.node,
};

export default function ThemeConfig({ children }) {
  const theme = createTheme({
    // typography: {
    //   fontFamily: "'Montserrat', sans-serif",
    //   fontWeightRegular: 600,
    //   fontSize: 12,
    // },
    typography,
    palette: {
      background: { default: '#fff', paper: '#fcfcfc' },
      primary: {
        main: '#1B5AFD',
        light: '#386FFD',
        contrastText: '#ffffff',
      },
      secondary: {
        lighter: '#353e56;',
        light: '#0F133C',
        main: '#090739;',
        contrastText: '#ffffff',
      },
    },
  });

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </StyledEngineProvider>
  );
}
