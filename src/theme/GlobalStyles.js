import { GlobalStyles as GlobalThemeStyles } from '@mui/material';
import { useTheme } from '@mui/material/styles';

export default function GlobalStyles() {
  const theme = useTheme();

  return (
    <GlobalThemeStyles
      styles={{
        '*': {
          boxSizing: 'border-box',
          margin: 0,
          padding: 0,
        },
        body: {
          fontFamily: '"Montserrat", sans-serif',
        },
        '.MuiTypography-root': {
          '&.MuiLink-root': {
            color: theme.palette.secondary.main,
            '&:active': {
              color: 'green',
            },
            '&:hover': {
              color: theme.palette.primary.light,
            },
          },
        },
      }}
    />
  );
}
