import TeamAptRouter from './routes';
import GlobalStyles from './theme/GlobalStyles';
import ThemeConfig from './theme/ThemeConfig';

const inputGlobalStyles = <GlobalStyles />;

export default function App() {
  return (
    <ThemeConfig>
      {inputGlobalStyles}
      <TeamAptRouter />
    </ThemeConfig>
  );
}
