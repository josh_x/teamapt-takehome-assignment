import avatar from '../static/mock/images/avatar/avatar_1.jpg';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ForumIcon from '@mui/icons-material/Forum';
import MenuIcon from '@mui/icons-material/Menu';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import { Breadcrumbs, Stack } from '@mui/material';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import IconButton from '@mui/material/IconButton';
import { styled } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import TABreadcrumb from '../components/TABreadcrumb';
import TAUserInfoCard from '../components/TAUserInfoCard';
import { Main } from '../styles/content.style';
import { MCAppBar } from '../styles/navbar.style';
import DashboardSidebar from './DashboardSidebar';

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const breadcrumbs = [
  <TABreadcrumb text="Projects" key={uuidv4()} />,
  <TABreadcrumb
    text="GSE Banking App"
    key={uuidv4()}
    sx={{ color: 'text.disabled' }}
  />,
];

DashboardWrapper.propTypes = {
  children: PropTypes.node,
};

export default function DashboardWrapper({ children }) {
  const [open, setOpen] = useState(true);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <MCAppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="default"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Stack spacing={2}>
            <Breadcrumbs
              separator={<ChevronRightIcon />}
              aria-label="breadcrumb"
            >
              {breadcrumbs}
            </Breadcrumbs>
          </Stack>

          <Stack
            sx={{ marginLeft: 'auto' }}
            direction="row"
            spacing={3}
            justifyContent="space-between"
          >
            <Stack direction="row">
              <IconButton>
                <ForumIcon color="action" />
              </IconButton>
              <IconButton>
                <NotificationsActiveIcon color="action" />
              </IconButton>
            </Stack>
            <TAUserInfoCard src={avatar} username="Finna A." />
          </Stack>
        </Toolbar>
      </MCAppBar>
      {<DashboardSidebar handleDrawerClose={handleDrawerClose} open={open} />}
      <Main open={open}>
        <DrawerHeader />
        {children}
      </Main>
    </Box>
  );
}
