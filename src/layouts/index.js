import { Outlet } from 'react-router-dom';

import DashboardWrapper from './DashboardWrapper';

export default function DashboardLayout() {
  return (
    <>
      <DashboardWrapper>
        <Outlet />
      </DashboardWrapper>
    </>
  );
}
