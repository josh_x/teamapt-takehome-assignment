import { Link as RouterLink } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import avatar from '../static/mock/images/avatar/avatar_1.jpg';
// icons
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';

// material
import {
  Drawer,
  IconButton,
  Link,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';

//others
import { useTheme } from '@mui/material/styles';
import PropTypes from 'prop-types';
import { SIDEBAR_MENUS } from '../utils/constants';

// styles
import { DrawerHeader } from '../styles/sidebar.style';
import { DRAWER_WIDTH } from '../utils/constants';
import { NewTask } from '../components/Task';
import TAUserInfoCard from '../components/TAUserInfoCard';

DashboardSidebar.propTypes = {
  handleDrawerClose: PropTypes.func.isRequired,
  open: PropTypes.bool,
};

export default function DashboardSidebar({ handleDrawerClose, open }) {
  const theme = useTheme();

  return (
    <Drawer
      sx={{
        width: DRAWER_WIDTH,
        flexShrink: 0,
        '& .MuiDrawer-paper': {
          borderColor: '#FBFBFB',
          width: DRAWER_WIDTH,
          boxSizing: 'border-box',
        },
      }}
      variant="persistent"
      anchor="left"
      open={open}
    >
      <DrawerHeader>
        <img
          src="https://teamapt.com/assets/img/svg/logo.svg"
          alt="TeamApt"
          style={{ width: '2rem' }}
        />

        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'ltr' ? (
            <ChevronLeftIcon />
          ) : (
            <ChevronRightIcon />
          )}
        </IconButton>
      </DrawerHeader>
      <List>
        {SIDEBAR_MENUS.map((menu) => (
          <Link to="/" underline="none" key={uuidv4()} component={RouterLink}>
            <ListItem button>
              <ListItemIcon>{menu.icon}</ListItemIcon>
              <ListItemText primary={menu.label} />
            </ListItem>
          </Link>
        ))}
      </List>

      <List>
        <ListItem>
          <NewTask
            text={
              <span>
                Create <br />
                new task
              </span>
            }
          />
        </ListItem>
      </List>

      <TAUserInfoCard email="finna@iksg.com" src={avatar} username="Finna A." />
    </Drawer>
  );
}
